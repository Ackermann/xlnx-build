"use strict";

var path = require("path");
var fs = require("fs");
var tools = require("./tools.js");

function runPlanAhead(config,callback) {
    //start_planahead.tcl erzeugen
    var planAheadDir = path.resolve(config.buildDir,"planahead");
    tools.mkdirIfNotExist(planAheadDir);
    var startScriptFileName = path.resolve(config.buildDir,"runplanahead.tcl");
    var deviceNamePlanAhead = config.deviceDetails.device+config.deviceDetails.devicePackage+"-"+config.deviceDetails.speedGrade;
    var startScript="create_project -name build -dir \""+planAheadDir+"\" -part "+deviceNamePlanAhead+"\r\n";
    startScript+="set srcset [get_property srcset [current_run -impl]]\r\n";
    startScript+="set_property design_mode GateLvl $srcset\r\n";
    startScript+="set_property edif_top_file \""+path.resolve(config.buildDir,config.target+".ngc")+"\" [ get_property srcset [ current_run ] ]\r\n";
    startScript+="add_files -norecurse { {"+config.buildDir+"} {ipcore_dir} }\r\n";
    startScript+="set_property target_constrs_file \""+config.constraintsFile+"\" [current_fileset -constrset]\r\n";
    startScript+="add_files [list {"+config.constraintsFile+"}] -fileset [get_property constrset [current_run]]\r\n";
    startScript+="link_design\r\n";
    startScript+="read_xdl -file \""+path.resolve(config.buildDir,config.target+".xdl")+"\"\r\n";
    startScript+="if {[catch {read_twx -name results_1 -file \""+path.resolve(config.buildDir,config.target+".twx")+"\"} eInfo]} {\r\n";
    startScript+="  puts \"WARNING: there was a problem importing \\\""+path.resolve(config.buildDir,config.target+".twx")+"\\\": $eInfo\"\r\n";
    startScript+="}\r\n";
    
    fs.writeFileSync(startScriptFileName,startScript);
    
    
    var runScript = "trce -intstyle xflow -v 3 -s 2 -n 3 -fastpaths -xml "+config.target+".twx "+config.target+".ncd -o "+config.target+".twr "+config.target+".pcf\n";
    runScript += "xdl -ncd2xdl "+config.target+".ncd "+config.target+".xdl\n";
    runScript += "planAhead -source \""+startScriptFileName+"\" -nolog\n";
    runScript += "rm -r \""+planAheadDir+"\"\n";
    tools.runISETool(config.buildDir,"runplanahead.sh",runScript,callback);
}

module.exports = {
    runPlanAhead:runPlanAhead
};
