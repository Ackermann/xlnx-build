"use strict";

var fs = require("fs");
var path = require("path");
var spawn = require("child_process").spawn;

function mergeConfiguration(defaultConfig,ownConfig) {
    var result = {};
    for (var key in defaultConfig) {
        if (defaultConfig.hasOwnProperty(key))
            result[key] = defaultConfig[key];
    }
    for (var key in ownConfig) {
        if (ownConfig.hasOwnProperty(key))
            result[key] = ownConfig[key];
    }
    return result;
}

function mkdirIfNotExist(dirname) {
    if (!fs.existsSync(dirname) || !fs.statSync(dirname).isDirectory())
        fs.mkdirSync(dirname);
}

function readGlobalSettings() {
    var result = JSON.parse(fs.readFileSync(path.resolve(__dirname,"defaults.json"),"utf-8"));
    result.devices = JSON.parse(fs.readFileSync(path.resolve(__dirname,"devices.json"),"utf-8"))
    return result;
}

function searchDevice(deviceIdentifier, silentError) {
    var m;
    if (!(m = deviceIdentifier.match(/^(x[A-Za-z0-9]+)-([0-9]+[A-Z]?)-([a-z0-9]+)$/))) {
        if (silentError)
            return null;
        throw new Error("illegal deviceIdentifier, format is device-speedgrade-package");
    }
    var device = m[1];
    var speedGrade = m[2];
    var devicePackage = m[3];
    for (var familyKey in module.exports.globalSettings.devices) {
        if (!module.exports.globalSettings.devices.hasOwnProperty(familyKey))
            continue;
        var f = module.exports.globalSettings.devices[familyKey];
        for (var deviceKey in f) {
            if (!f.hasOwnProperty(deviceKey) || deviceKey !== device)
                continue;
            var d = f[deviceKey];
            for (var k=0; k<d.length; ++k) {
                if (d[k][0] === speedGrade && d[k][1] === devicePackage)
                    return {
                        family:familyKey,
                        device:device,
                        speedGrade:speedGrade,
                        devicePackage:devicePackage
                    };
            }
        }
    }
    if (silentError)
        return null;
    throw new Error("no matching device '"+device+"' with speed grade '"+speedGrade+"' and package '"+devicePackage+"' found");
}

module.exports = {
    globalSettings: readGlobalSettings(),
    mkdirIfNotExist: mkdirIfNotExist,
    getLastModifiedDate:function(filename) {
        if (!fs.existsSync(filename))
            return 0;
        return fs.statSync(filename).mtime.getTime();
    },
    getNewerFileVersion:function(leftFilename,rightFilename) {
        var l = module.exports.getLastModifiedDate(leftFilename);
        var r = module.exports.getLastModifiedDate(rightFilename);
        if (l === r)
            return "None";
        return l>r ? "Left" : "Right";
    },
    getVHDLFileForPSHDL:function(buildDir,pshdlFile) {
        return path.resolve(buildDir,"pshdl-build",path.basename(pshdlFile,path.extname(pshdlFile))+".vhdl");
    },
    readBuildFile:function(fileName) {
        var cfg = mergeConfiguration({
            "sourceDir":"",
            "buildDir":"build",
            "vhdlFiles":[],
            "pshdlFiles":[],
            "xcoFiles":[],
            "constraintsFile":"constraints.ucf",
            "customXstConfiguration": {},
            "customBitFileConfiguration": {},
            "target": "main",
            "topComponent": "main",
            "psdhlIncludeLib" : true
        },JSON.parse(fs.readFileSync(fileName,"utf-8")));
        cfg.sourceDir = path.resolve(cfg.sourceDir);
        cfg.buildDir = path.resolve(cfg.buildDir);
        cfg.customXstConfiguration = mergeConfiguration(module.exports.globalSettings.defaultXstConfiguration,cfg.customXstConfiguration);
        cfg.customBitFileConfiguration = mergeConfiguration(module.exports.globalSettings.defaultBitFileConfiguration,cfg.customBitFileConfiguration);
        
        //device id parsen
        if (!("deviceIdentifier" in cfg))
            throw new Error("missing 'deviceIdentifier' in build file");
        
        
        cfg.deviceDetails = searchDevice(cfg.deviceIdentifier,false);
        
        for (var i=0; i<cfg.vhdlFiles.length; ++i) {
            cfg.vhdlFiles[i] = path.resolve(cfg.sourceDir,cfg.vhdlFiles[i]);
        }
        for (var i=0; i<cfg.pshdlFiles.length; ++i) {
            cfg.pshdlFiles[i] = path.resolve(cfg.sourceDir,cfg.pshdlFiles[i]);
            cfg.vhdlFiles.push(module.exports.getVHDLFileForPSHDL(cfg.buildDir,cfg.pshdlFiles[i]));
        }
        if (cfg.pshdlFiles.length > 0 && cfg.psdhlIncludeLib)
            cfg.vhdlFiles.push(module.exports.globalSettings.pshdlLib);
        //nicht jede xco komponente erzeugt eine vhd datei, das wird deshalb nach der ipcore build geprüft (findMatchingVHDAndNGCFiles)
        /*for (var i=0; i<cfg.xcoFiles.length; ++i) {
            cfg.vhdlFiles.push(path.resolve(cfg.buildDir,"ipcore_dir",path.basename(cfg.xcoFiles[i],path.extname(cfg.xcoFiles[i]))+".vhd"));
        }*/
        cfg.constraintsFile = path.resolve(cfg.sourceDir,cfg.constraintsFile);
        
        return cfg;
    },
    buildDirs:function(config) {
        mkdirIfNotExist(config.buildDir);
        config.ipCoreDir = path.resolve(config.buildDir,"ipcore_dir");
        mkdirIfNotExist(config.ipCoreDir);
        mkdirIfNotExist(path.resolve(config.buildDir,"xst-projnav.tmp"));
    },
    searchDevice:searchDevice,
    runISETool:function(baseDir,runScriptFileName,runScript,callback) {
        //run file erzeugen
        var runFile = "#!/bin/bash\n";
        runFile += "set -e\n";
        runFile += "source \""+module.exports.globalSettings.iseSettingsFile+"\"\n";
        runFile += "set -e\n";
        runFile += "cd \""+baseDir+"\"\n";
        runFile += runScript;
        
        var runScriptFileName = path.resolve(baseDir,runScriptFileName);
        fs.writeFileSync(runScriptFileName,runFile);
        fs.chmodSync(runScriptFileName,7*8*8);

        
        var buildProcess = spawn(runScriptFileName, [], {
            stdio:["pipe",process.stdout,process.stderr]
        });

        buildProcess.on("close", function (code) {
            if (code === 0) {
                callback();
                return;
            }
            console.log("Errors detected");
            process.exit(1);
        });
    }
}
