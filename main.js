"use strict";

var program = require("commander");
var tools = require("./tools.js");
var coregen = require("./coregen.js");
var build = require("./build.js");
var planAhead = require("./planahead.js");
var isim = require("./isim.js");
var fs = require("fs");
var path = require("path");

function success() {
    console.log("SUCCESS - YEAH!");
}

function compileAndTranslateMapRoute(config,callback) {
    build.runPSHDL(config,function() {
        coregen.generateIpCores(config,function() {
            build.translateMapRoute(config,callback);
        });
    });
}

program
  .version("0.0.1")
  .option("-b, --build-file [file]", "Set Buildfile (default: build.json)","build.json");
  
program.command("build-pshdl")
    .description("run pshdl generator only")
    .action(function(){
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        build.runPSHDL(config,success);
    });
    
program.command("build-ipcore")
    .description("run ipcore generator only")
    .action(function(){
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        coregen.generateIpCores(config,success);
    });
  
program.command("build-all")
    .description("run a full build")
    .action(function(){
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        compileAndTranslateMapRoute(config,function() {
            build.generateBitFile(config,success);
        });
    });

program.command("coregen [xcofile]")
    .description("runs xilinx coregen tool to manage ip core, specify a xco file if you want to change an existing core")
    .action(function(xcofile) {
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        coregen.openGui(config,xcofile);
    });
    
program.command("plan-ahead-timing")
    .description("starts plan ahead with timing diagnostics")
    .action(function() {
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        compileAndTranslateMapRoute(config,function() {
            planAhead.runPlanAhead(config,success);
        });
    });
    
program.command("test-isim <unit>")
    .description("run a testbench unit")
    .action(function(unit) {
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        build.runPSHDL(config,function() {
            build.createProjectFile(config);
            isim.runISim(config,unit,success);
        });
    });
    
program.command("show-device-families")
    .description("shows all device families")
    .action(function() {
        var allFamilies = {"spartan":[],"zynq":[],"artix":[],"kintex":[],"virtex":[],"other":[]};
        for (var key in tools.globalSettings.devices) {
            if (tools.globalSettings.devices.hasOwnProperty(key)) {
                var found = false;
                for (var masterFamily in allFamilies) {
                    if (allFamilies.hasOwnProperty(masterFamily) && key.indexOf(masterFamily)>=0) {
                        found = true;
                        allFamilies[masterFamily].push(key);
                        break;
                    }
                }
                if (!found)
                    allFamilies["other"].push(key);
            }
        }
        for (var masterFamily in allFamilies) {
            if (allFamilies.hasOwnProperty(masterFamily)) {
                var f = allFamilies[masterFamily];
                if (f.length > 0) {
                    console.log(masterFamily+":");
                    for (var i=0; i<f.length; ++i)
                        console.log("  - "+f[i]);
                }
            }
        }
    });
    
program.command("show-devices <family> [device]")
    .description("shows all devices of a family or all package/speed grades of a particular device (if specified)")
    .action(function(family,device) {
        if (!tools.globalSettings.devices.hasOwnProperty(family)) {
            console.log("No family '"+family+"' found. Use 'xlnx-build show-device-families' to list all available families");
            process.exit(1);
        }
        var f = tools.globalSettings.devices[family];
        if (typeof device === "undefined") {
            for (var key in f) {
                if (f.hasOwnProperty(key)) {
                    console.log(key);
                }
            }
            return;
        }
        if (!f.hasOwnProperty(device)) {
            console.log("Invalid device '"+device+"' given. Use 'xlnx-build show-devices "+family+"' to list all devices of the family '"+family+"'");
            process.exit(1);
        }
        console.log("Speed\t\tPackage\t\tFull device specifier used in build.json");
        console.log("************************************************************************");
        var lst = f[device];
        for (var i=0; i<lst.length; ++i) {
            console.log(lst[i][0]+"\t\t"+lst[i][1]+"\t\t"+device+"-"+lst[i][0]+"-"+lst[i][1]);
        }
    });
    
program.command("init [device]")
    .description("generates a build.json file")
    .option("--full", "includes all options in generated file")
    .option("--stdout", "instead of writing the file 'build.json', send contents to stdout")
    .action(function(device,options) {
        if (fs.existsSync("build.json") && !options.stdout) {
            console.log("There is already a file called 'build.json', delete it or run this command again with the --stdout option");
            process.exit(1);
        }
        var directory = fs.readdirSync(path.resolve(""));
        
        var config = {
            vhdlFiles:[],
            pshdlFiles:[],
            xcoFiles:[],
            constraintsFile: "constraints.ucf",
            deviceIdentifier: "TODO: use 'xlnx-build show-device-families' and 'xlnx-build show-devices <family> [device]' to find your device"
        };
        
        if (typeof device !== "undefined") {
            if (tools.searchDevice(device,true) === null) {
                console.log("Illegal device given, use 'xlnx-build show-device-families' and 'xlnx-build show-devices <family> [device]' to find your device");
                process.exit(1);
            }
            config.deviceIdentifier = device;
        }
        
        if (options.full) {
            config.customXstConfiguration = {};
            config.customBitFileConfiguration = {};
            config.sourceDir = "";
            config.buildDir = "build";
            config.target = "main";
            config.topComponent = "main";
            config.psdhlIncludeLib = true;
        }
        
        for (var i=0; i<directory.length; ++i) {
            var ext = path.extname(directory[i]);
            if (ext === ".vhd" || ext === ".vhdl")
                config.vhdlFiles.push(directory[i]);
            else if (ext === ".pshdl")
                config.pshdlFiles.push(directory[i]);
            else if (ext === ".xco")
                config.xcoFiles.push(directory[i]);
        }
        var config = JSON.stringify(config,null,4);
        if (options.stdout)
            console.log(config);
        else
            fs.writeFileSync("build.json",config,"utf-8");
    });
    
program.command("flash <programmer>")
    .description("build and then flash bit file to fpga")
    .action(function(programmer) {
        var config = tools.readBuildFile(program.buildFile);
        tools.buildDirs(config);
        compileAndTranslateMapRoute(config,function() {
            build.generateBitFile(config,function() {
                build.flashBitFile(config,programmer,success);
            });
        });
    });
    
program.command("show-programmers")
    .description("shows all available programmers for the 'flash' command")
    .option("--verbose","show programmer command")
    .action(function(options) {
        for (var name in tools.globalSettings.programmer) {
            if (tools.globalSettings.programmer.hasOwnProperty(name)) {
                console.log(name);
                if (options.verbose) {
                    var p = tools.globalSettings.programmer[name];
                    console.log("    Binary:         "+p.binary);
                    console.log("    Arguments:      "+JSON.stringify(p.arguments));
                }
            }
        }
    });

program.command("*","",{noHelp:true})
   .action(function(env) {
       console.log("Invalid sub command '"+env+"'");
       program.help();
   });

program.parse(process.argv);

if(!program.args.length)
    program.help();
