xlnx-build tool
===============

Overview
--------

This tool simplifies the process of building FPGA projects using the XILINX(R) ISE Webpack
toolchain.

Setup of xlnx-build
-------------------

1. Install XILINX ISE Webpack
    * Under linux you may have to change the settings64.sh or settings32.sh file in the 
      xilinx root directory as this crashes some times. Add a `unset QT_PLUGIN_PATH`,
      `unset KDE_SESSION_VERSION` maybe change LD_PRELOAD. For more details see
      https://forums.xilinx.com/t5/Installation-and-Licensing/Xilinx-ISE-segmentation-fault-solution/td-p/209559
2. Install PSHDL Compiler (only required if your project is using PSHDL)
3. Install node and npm (`aptitude install nodejs npm`)
4. In the xlnx-build main directory run `npm install`.
5. In the xlnx-build main directory copy defaults.json.example to defaults.json and change to following entries:
    * Set "iseSettingsFile" to the path of the settings sh script file 
      e.g. "/opt/Xilinx/14.7/ISE_DS/settings64.sh"
    * Set "pshdlJar" to the jar file ot the pshdl compiler (only if you are using pshdl)
      eg. "/opt/pshdl/pshdl.jar"
    * Set "pshdlLib" to the path of "pshdl_pkg.vhd" (only if you are using pshdl)
      eg. "/opt/pshdl/pshdl_pkg.vhd"
    * If you want to use a programmer to flash your files to the fpga
      you can also define on in the "progammer" sections
      for more details on this see below
6. Create a file "/usr/bin/xlnx-build" with the following content
    ```sh
    #!/bin/bash
    node "/path/to/your/xlnx-build/main.js" "$@"
    ```
7. Make this file executable: `chmod +x /usr/bin/xlnx-build`
8. Test this tool by just typing `xlnx-build`. It should display a short help screen

Starting a new project
----------------------

Before starting a project you should find your target device. There are a lot of different
devices offered by XILINX, so you can search it recursivly in three steps. If you
already know your exact device name, you can omit this step.

Run `xlnx-build show-device-families` to get a list of all available XILINX families. 
After you found your family, search the device by `xlnx-build show-devices <family>` 
(e.g. `xlnx-build show-devices spartan6`).

This device is offered in different packages and speed grades.
Run `xlnx-build show-devices <family> <device>` to get a list of the device identifier
(e.g. `xlnx-build show-devices spartan6 xc6slx9`). Copy the full device identifier for
later use (e.g. "xc6slx9-2-csg225").

After you found your device you can create the build.json by yourself or just type
`xlnx-build init <device>` (e.g. `xlnx-build init xc6slx9-2-csg225`) to let xlnx-build
create a build.json for you.

Configuration by build.json
---------------------------

The whole project is configured by the build.json file. You can see an example below.

The most important parameters are:

* "deviceIdentifier": Set the full device identifier as described in the previous section
* "constraintsFile": You constraints filename, you need exactly one file.
* "pshdlFiles": List all of your PSHDL Files here, if you don't use it, leave it empty
* "vhdlFiles": List all your VHDL Files here (do not list auto generated files by pshdl)
* "xcoFiles": List all XILINX IP Core descriptor files here (see on this later)
* "target": Specify the name of the target binary file (without extension)
* "topComponent": Specifiy the name of the outer most component
* "psdhlIncludeLib": Set this to true if you are using PSHDL, otherwise to false
* "buildDir": Set this to the build subdirectory (default "build"). All build related 
              temporary files and the final binary file will be placed inside this directory.
              Your source directory will be left clean.

```json
{
    "vhdlFiles": [
        "reset_generator.vhd"
    ],
    "pshdlFiles": [
        "main.pshdl","vgaport.pshdl","uart_receiver.pshdl","uart_sender.pshdl","cpuenvironment.pshdl","cpucore.pshdl","cpualu.pshdl","cpubranchunit.pshdl"
    ],
    "xcoFiles": [
        "clockmanager.xco","characterrom.xco","mainmemory.xco"
    ],
    "constraintsFile": "constraints.ucf",
    "deviceIdentifier": "xc6slx9-2-tqg144",
    "customXstConfiguration": {
        "keep_hierarchy":"Yes"
    },
    "customBitFileConfiguration": {},
    "sourceDir": "",
    "buildDir": "build",
    "target": "main",
    "topComponent": "main",
    "psdhlIncludeLib": true
}
```

Building your project
---------------------

After your project has been defined, you can build your project by `xlnx-build build-all`.
If you only want to compile pshdl files to VHDL then just run `xlnx-build build-pshdl`.
If you only want to compile the ip core files to VHDL then just run `xlnx-build build-ipcore`.

You can also flash the binary file to your FPGA. Just type `xlnx-build flash <programmer>`.
This will first build the whole project and then flash it to your FPGA by the specified
programmer tool. To get a list of all supported programmers, type `xlnx-build show-programmers`.

To add a programmer, you have to edit the "defaults.json" in the xlnx-build main directory.
Search for the "programmer" section:

```json
    "programmer": {
        "papilio-fpga" : {
            "binary": "/opt/papilio-loader/programmer/linux32/papilio-prog",
            "arguments": ["-v", "-f" ,"$(bitfile)"]
        }
    }

```

In this example a programmer called "papilio-fpga" is defined, which will be used in the 
"flash" command. For each programmer you have to specify the full bath to the binary and a list of
command line arguments. The string "$(bitfile)" in the arguments will be replaced by
xlnx-build with the full path of the bitfile.

Using Coregen
-------------

Sometimes you want to include special components offered by XILINX that are not written 
in VHDL or PSHDL to your project. One example would be the PLL/Clocksource. This is done
by a XILINX GUI tool called XILINX Core Generator. All options are specified in a .xco file and later
generated to VHDL. 

To add a new component, just add the new (not yet existing) filename 
in your build.json file in the "xcoFiles" list. Then just run `xlnx-build coregen`. After some time the XILINX tool will launch and you can select the type of the component.
After this you have to specify a component name. Please use the same name as specified in
the xcoFiles (without .xco extension). When you finished your settings, a new xco file will
be generated. It will be used in future builds.

To change an existing component, just type `xlnx-build coregen <xcofile>`. After some time
the Core Generator wizard will show up again. Please note: you cannot change the type of the
component any more. If you want to do this, delete the xco filename and proceed as in then
previous paragraph.

Using XILINX Plan Ahead
-----------------------

When you run into timing problems, a tool called XILINX Plan Ahead can be very useful.
You can start this by running `xlnx-build plan-ahead-timing`.

Legal
-----

Xilinx, Core Generator, Plan Ahead, Spartan, ISE are tradmarks of Xilinx Inc. This tool (xlnx-build) is not related 
to Xilinx Inc. in any way and is published under the MIT License (see LICENSE file in main directory). The xlnx-build 
tool calls the Xilinx ISE Toolchain and you may need a license in order to use them.