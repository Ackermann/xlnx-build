"use strict";

var path = require("path");
var fs = require("fs");
var fsextra = require("fs-extra");
var tools = require("./tools.js");

//Synchronisiert die XCO Dateien aus dem Source Verzeichnis in das ipcore_dir (bei direction === "SourceToBuild") oder anders
//herum bei direction === "BuildToSource"

function verifyXCOFile(filename) {
    var lines = fs.readFileSync(filename, "utf8").replace(/\r/g,"").split("\n");
    var selectFound = false;
    var componentNameFound = "";
    for (var i=0; i<lines.length; ++i) {
        if (lines[i].substr(0,7) === "SELECT ")
            selectFound = true;
        else {
            var m = lines[i].match(/CSET component_name *= *(.*)/);
            if (m)
                componentNameFound = m[1];
        }
    }
    
    if (!selectFound)
        throw new Error("invalid xco file '"+filename+"', missing 'SELECT' instruction");
    if (componentNameFound === "")
        throw new Error("invalid xco file '"+filename+"', missing 'CSET component_name=' instruction");
    var expectedComponentName = path.basename(filename,path.extname(filename));
    if (componentNameFound !== expectedComponentName)
        throw new Error("invalid xco file '"+filename+"', expected a component_name of '"+expectedComponentName+"', got '"+componentNameFound+"'");
}

function syncXCOFiles(ipCoreDir, sourceDir, xcoFiles, direction) {
    var changedFiles = [];
    for (var i=0; i<xcoFiles.length; ++i) {
        var fileNameInSourceDir = path.resolve(sourceDir,xcoFiles[i]);
        var fileNameInBuildDir = path.resolve(ipCoreDir,path.basename(xcoFiles[i]));
        var newerFile = tools.getNewerFileVersion(fileNameInSourceDir,fileNameInBuildDir);
        if (direction === "BuildToSource" && newerFile === "Right") {
            verifyXCOFile(fileNameInBuildDir);
            changedFiles.push([fileNameInBuildDir,fileNameInSourceDir]);
        }
        else if (direction === "SourceToBuild" && newerFile === "Left") {
            verifyXCOFile(fileNameInSourceDir);
            changedFiles.push([fileNameInSourceDir,fileNameInBuildDir]);
        }
    }
    
    //console.log("CHANGED XCO FILES: "+direction);
    //console.log(changedFiles);
    
    var changedFilesBaseNames=[];
    for (var i=0; i<changedFiles.length; ++i) {
        fsextra.copySync(changedFiles[i][0],changedFiles[i][1]);
        
        //Datum Ziel := Datum Quelle
        var tmp = fs.statSync(changedFiles[i][0]);
        fs.utimesSync(changedFiles[i][1], tmp.atime, tmp.mtime);
        
        changedFilesBaseNames.push(path.basename(changedFiles[i][0]));
    }
    return changedFilesBaseNames;
}

function findMatchingVHDAndNGCFiles(ipCoreDir, xcoFiles, vhdlFiles) {
    for (var i=0; i<xcoFiles.length; ++i) {
        var baseName = path.resolve(ipCoreDir,path.basename(xcoFiles[i],path.extname(xcoFiles[i])));
        try {
            fs.lstatSync(baseName+".vhd");
            vhdlFiles.push(baseName+".vhd");
        } catch (e) {}
    }
}

//Aktualisiert IP Cores, deren XCO im Sourceverzeichnis aktualisiert wurden (z.B. durch repository update)
function updateChangedSourceFiles(ipCoreDir, sourceDir, xcoFiles, deviceDetails, vhdlFiles, callback) {
    var changedFiles = syncXCOFiles(ipCoreDir, sourceDir, xcoFiles, "SourceToBuild");
    if (changedFiles.length === 0) {
        findMatchingVHDAndNGCFiles(ipCoreDir,xcoFiles,vhdlFiles);
        callback();
        return;
    }
    //coregen.batch erzeugen
    var coregenBatchFile = "";
    for (var i=0; i<changedFiles.length; ++i)
        coregenBatchFile += "EXECUTE "+changedFiles[i]+"\r\n";
    var coregenBatchFileName = path.resolve(ipCoreDir,"coregen.batch");
    fs.writeFileSync(coregenBatchFileName,coregenBatchFile);
    runCoreGen(ipCoreDir," -b \""+coregenBatchFileName+"\"",deviceDetails,callback);
    findMatchingVHDAndNGCFiles(ipCoreDir,xcoFiles,vhdlFiles);
}

function runCoreGen(ipCoreDir,extraCommands,deviceDetails,callback) {
    //coregen.cgp erzeugen
    var coregenProj="";
    
    coregenProj += "SET busformat = BusFormatAngleBracketNotRipped\r\n";
    coregenProj += "SET designentry = VHDL\r\n";
    coregenProj += "SET flowvendor = Other\r\n";
    coregenProj += "SET device = "+deviceDetails.device+"\r\n";
    coregenProj += "SET devicefamily = "+deviceDetails.family+"\r\n";
    coregenProj += "SET package = "+deviceDetails.devicePackage+"\r\n";
    coregenProj += "SET speedgrade = -"+deviceDetails.speedGrade+"\r\n";
    coregenProj += "SET verilogsim = false\r\n";
    coregenProj += "SET vhdlsim = true\r\n";
    
    coregenProj += "SET addpads = false\r\n";
    coregenProj += "SET asysymbol = true\r\n";
    coregenProj += "SET createndf = false\r\n";
    coregenProj += "SET formalverification = false\r\n";
    coregenProj += "SET foundationsym = false\r\n";
    coregenProj += "SET implementationfiletype = Ngc\r\n";
    coregenProj += "SET removerpms = false\r\n";
    coregenProj += "SET simulationfiles = Behavioral\r\n";
    coregenProj += "SET workingdirectory = ./tmp/\r\n";
    
    
    fs.writeFileSync(path.resolve(ipCoreDir,"coregen.cgp"),coregenProj);
    
    tools.runISETool(ipCoreDir,"runcoregen.sh","coregen -p coregen.cgp"+extraCommands+"\n",callback);
}

function openGui(config,editFileName) {
    function syncBack() {
        syncXCOFiles(config.ipCoreDir, config.sourceDir, config.xcoFiles, "BuildToSource");
    }
    
    updateChangedSourceFiles(config.ipCoreDir, config.sourceDir, config.xcoFiles, config.deviceDetails, config.vhdlFiles, function() {
        if (typeof editFileName === "string") {
            editFileName = path.resolve(config.ipCoreDir,path.basename(editFileName));
            if (!fs.existsSync(editFileName))
                throw new Error("xco file '"+editFileName+"' was not found");
            //coregen.batch erzeugen
            var coregenBatchFileName = path.resolve(config.ipCoreDir,"coregen.batch");
            fs.writeFileSync(coregenBatchFileName,"LAUNCHXCO "+editFileName);
            runCoreGen(config.ipCoreDir," -b \""+coregenBatchFileName+"\"",config.deviceDetails,syncBack);
        }
        else
            runCoreGen(config.ipCoreDir,"",config.deviceDetails,syncBack);
    });
}

function generateIpCores(config,callback) {
    updateChangedSourceFiles(config.ipCoreDir, config.sourceDir, config.xcoFiles, config.deviceDetails, config.vhdlFiles, callback);
}

module.exports = {
    openGui:openGui,
    generateIpCores:generateIpCores
};
