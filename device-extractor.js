"use strict";

var fs = require("fs");
var lines = fs.readFileSync("/opt/Xilinx/14.7/ISE_DS/ISE/data/coregen/pa_cg_catalog.xml","utf-8").replace(/\r/g,"").split("\n");

var familyRe = / *<Family.*name *= *"([^"]*)"/;
var partRe = / *<Part.*name *= *"([^"]*)"/;
var stopFamilyRe = / *<\/Family/;
var numberRe = /^(x[A-Za-z0-9]+)-([0-9]+[A-Z]?)([a-z0-9]+)$/;

var currentFamily = null;

var familyMap = {};

for (var i=0; i<lines.length; ++i) {
    var m;
    
    if (m = lines[i].match(familyRe)) {
        currentFamily = m[1];
    }
    else if (m = lines[i].match(stopFamilyRe)) {
        currentFamily = null;
    }
    else if (m = lines[i].match(partRe)) {
        var name = m[1];
        if (name === "ALL")
            continue;
        if (currentFamily === null)
            throw new Error("part out of family at line "+i);
        
        if (!(m = name.match(numberRe)))
            throw new Error("part number does not match "+name);
        
        if (!(currentFamily in familyMap))
            familyMap[currentFamily] = {};
        
        var device = m[1];
        
        var f = familyMap[currentFamily];
        if (!(device in f))
            f[device] = [];
        
        var lst = f[device];
        
        var found = false;
        for (var k=0; k<lst.length; ++k) {
            if (lst[k][0] === m[2] && lst[k][1] === m[3]) {
                found = true;
                break;
            }
        }
        if (!found)
            lst.push([m[2],m[3]]); //speed & package
        
        //console.log(m[1]+" "+m[2]+" "+m[3]);
        
        
    }
}

var result = "{";
var firstFamily = true;
for (var fkey in familyMap) {
    if (!familyMap.hasOwnProperty(fkey))
        continue;
    if (!firstFamily)
        result += ",";
    firstFamily = false;
    result += "\r\n    \""+fkey+"\": {";
    var f = familyMap[fkey];
    var firstDevice = true;
    for (var dkey in f) {
        if (!f.hasOwnProperty(dkey))
            continue;
        if (!firstDevice)
            result += ",";
        firstDevice = false;
        result += "\r\n        \""+dkey+"\": [";
        var l = f[dkey];
        for (var i=0; i<l.length; ++i) {
            if (i>0)
                result += ",";
            result += "\r\n            "+JSON.stringify(l[i]);
        }
        result += "\r\n        ]";
    }
    result += "\r\n    }";
}
result += "\r\n}\r\n";

fs.writeFileSync("devices.json",result);