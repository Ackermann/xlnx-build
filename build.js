"use strict"; 
var spawn = require("child_process").spawn;
var path = require("path");
var fs = require("fs");
var tools = require("./tools.js");

function createProjectFile(config) {
    var prjFile = "";
    for (var i=0; i<config.vhdlFiles.length; ++i) {
        prjFile += "vhdl work \""+config.vhdlFiles[i]+"\"\r\n";
    }
    fs.writeFileSync(path.resolve(config.buildDir,config.target+".prj"),prjFile);
}

function translateMapRoute(config,callback) {
    var compileRequired = false;
    var destFileName = path.resolve(config.buildDir,config.target+".ncd");
    for (var i=0; i<config.vhdlFiles.length; ++i) {
        if (tools.getNewerFileVersion(config.vhdlFiles[i],destFileName) === "Left") {
            compileRequired = true;
        }
    }
    if (!compileRequired) {
        callback();
        return;
    }
    
    //project.xst erzeugen
    var xstConfiguration = config.customXstConfiguration;
    xstConfiguration["ifn"] = config.target+".prj";
    xstConfiguration["ofn"] = config.target;
    xstConfiguration["top"] = config.topComponent;
    xstConfiguration["ofmt"] = "NGC";
    xstConfiguration["p"] = config.deviceIdentifier;
    /*if (config.xcoFiles.length > 0)
        xstConfiguration["sd"] = "{\"ipcore_dir\"}";*/

    var xstConfigurationFile = "set -tmpdir \"xst-projnav.tmp\"\r\nset -xsthdpdir \"xst\"\r\nrun\r\n";
    for (var key in xstConfiguration) {
        if (xstConfiguration.hasOwnProperty(key))
            xstConfigurationFile += "-"+key+" "+xstConfiguration[key]+"\r\n";
    }
    fs.writeFileSync(path.resolve(config.buildDir,config.target+".xst"),xstConfigurationFile);

    //project.prj erzeugen
    createProjectFile(config);

    //translate_map_route.sh erzeugen
    var buildFile = "xst -intstyle xflow -ifn  \""+config.target+".xst\" -ofn \""+config.target+".syr\"\n";
    buildFile += "ngdbuild -intstyle xflow -dd _ngo -sd \""+path.resolve(config.buildDir,"ipcore_dir")+"\" -nt timestamp -uc \""+config.constraintsFile+"\" -p \""+config.deviceIdentifier+"\" "+config.target+".ngc "+config.target+".ngd\n";
    buildFile += "map -intstyle xflow -p \""+config.deviceIdentifier+"\" -w -logic_opt off -ol high -t 1 -xt 0 -register_duplication off -r 4 -global_opt off -mt off -ir off -pr off -lc off -power off -o "+config.target+"_map.ncd "+config.target+".ngd "+config.target+".pcf\n";
    buildFile += "par -w -intstyle xflow -ol high -mt off "+config.target+"_map.ncd "+config.target+".ncd "+config.target+".pcf\n";

    tools.runISETool(config.buildDir,"translate_map_route.sh",buildFile,callback);
}

function generateBitFile(config,callback) {
    if (tools.getNewerFileVersion(path.resolve(config.buildDir,config.target+".ncd"),path.resolve(config.buildDir,config.target+".bit")) !== "Left") {
        callback();
        return;
    }
    //project.ut erzeugen
    var bitFileConfiguration = config.customBitFileConfiguration;
    var bitFile = "-w\r\n";
    for (var key in bitFileConfiguration) {
        if (bitFileConfiguration.hasOwnProperty(key))
            bitFile += "-g "+key+":"+bitFileConfiguration[key]+"\r\n";
    }
    fs.writeFileSync(path.resolve(config.buildDir,config.target+".ut"),bitFile);
    tools.runISETool(config.buildDir,"generate_bit_file.sh","bitgen -intstyle xflow -f "+config.target+".ut "+config.target+".ncd\n",callback);
}

function runPSHDL(config,callback) {
    var compileRequired = false;
    for (var i=0; i<config.pshdlFiles.length; ++i) {
        if (tools.getNewerFileVersion(config.pshdlFiles[i],tools.getVHDLFileForPSHDL(config.buildDir,config.pshdlFiles[i])) === "Left") {
            compileRequired = true;
        }
    }
    if (!compileRequired) {
        callback();
        return;
    }
    
    
    var pshdlParameters = ["-jar",tools.globalSettings.pshdlJar,"vhdl","-o",path.resolve(config.buildDir,"pshdl-build")].concat(config.pshdlFiles);
    var buildProcess = spawn("java", pshdlParameters, {
        stdio:["pipe",process.stdout,process.stderr]
    });
    buildProcess.on("close", function (code) {
        if (code === 0) {
            callback();
            return;
        }
        console.log("PSHDL Compiler failed");
        process.exit(1);
    });
}

function flashBitFile(config,programmer,callback) {
    if (!tools.globalSettings.programmer.hasOwnProperty(programmer))
        throw new Error("No programmer '"+programmer+"' found");
    
    programmer = tools.globalSettings.programmer[programmer];
    for (var i=0; i<programmer.arguments.length; ++i)
        programmer.arguments[i] = programmer.arguments[i].replace(/\$\(bitfile\)/g,path.resolve(config.buildDir,config.target+".bit"));
                    
    var flashProcess = spawn(programmer.binary, programmer.arguments, {
        stdio:["pipe",process.stdout,process.stderr]
    });

    flashProcess.on("close", function (code) {
        if (code === 0) {
            callback();
            return;
        }
        console.log("Errors detected");
        process.exit(1);
    });
}

module.exports = {
    runPSHDL:runPSHDL,
    translateMapRoute:translateMapRoute,
    generateBitFile:generateBitFile,
    flashBitFile:flashBitFile,
    createProjectFile:createProjectFile
};

